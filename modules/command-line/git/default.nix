{pkgs, ...}: {
  programs = {
    git = {
      enable = true;
      userEmail = "ltremble@uoguelph.ca";
      userName = "ltremble";
    };
  };
}
