{
  services = {
    xserver = {
      desktopManager.plasma5.enable = true;
      displayManager.sddm.enable = true;
      enable = true;
      layout = "us";
    };
  };
}
