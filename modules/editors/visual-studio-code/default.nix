{pkgs, ...}: {
  programs = {
    vscode = {
      enable = true;
      package = pkgs.vscodium;
      extensions = with pkgs.vscode-extensions; [
        elmtooling.elm-ls-vscode
        haskell.haskell
        james-yu.latex-workshop
        jnoortheen.nix-ide
        justusadam.language-haskell
        kamadorueda.alejandra
        ms-vscode.cpptools
        pkief.material-icon-theme
        redhat.vscode-yaml
        tamasfe.even-better-toml

        # School
        xaver.clang-format

        # Test
        gruntfuggly.todo-tree
      ];
    };
  };
}
