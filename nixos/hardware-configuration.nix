{
  config,
  lib,
  modulesPath,
  pkgs,
  ...
}: {
  boot = {
    extraModulePackages = [];
    initrd.kernelModules = [];
  };

  networking.useDHCP = lib.mkDefault true;

  swapDevices = [];
}
