{
  config,
  lib,
  modulesPath,
  pkgs,
  ...
}: {
  boot = {
    initrd.availableKernelModules = [ "ata_piix" "ohci_pci" "sd_mod" "sr_mod" ];
    kernelModules = [];
  };

  fileSystems."/" =
    { device = "/dev/disk/by-uuid/3a54556a-da15-494c-807d-429e32eea1bb";
      fsType = "ext4";
    };

  hardware.cpu.intel.updateMicrocode = lib.mkDefault config.hardware.enableRedistributableFirmware;          

    virtualisation.virtualbox.guest.enable = true;                                                                                                                                            

  imports = [
    (modulesPath + "/installer/scan/not-detected.nix")
  ];
}
