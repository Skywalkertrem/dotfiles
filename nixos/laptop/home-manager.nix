{pkgs, ...}: {
  home = {
    packages = with pkgs; [
      flameshot
      vlc
      
      # School
      octave
      speedcrunch
    ];
  };

  imports = import ../../modules/internet;
}
