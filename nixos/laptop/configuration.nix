{
  config,
  nixpkgs,
  pkgs,
  user,
  ...
}: {
  boot.loader.grub = {
    enable = true;
    device = "/dev/sda";
    useOSProber = true;
  };

  hardware.bluetooth.enable = true;

  networking.hostName = "laptop";
}
