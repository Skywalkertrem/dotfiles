{
  home-manager,
  nixpkgs,
  user,
}: let
  specialArgs = {
    inherit nixpkgs;
    inherit user;
  };
  system = "x86_64-linux";
  modulesFunc = homeModules: systemModules: ([
      ./configuration.nix
      ./hardware-configuration.nix

      home-manager.nixosModules.home-manager
      {
        home-manager = {
          extraSpecialArgs = {
            inherit user;
          };
          useGlobalPkgs = true;
          users.${user} = {
            imports =
              [(import ./home-manager.nix)]
              ++ homeModules;
          };
          useUserPackages = true;
        };
      }
    ]
    ++ systemModules);
in {
  desktop = nixpkgs.lib.nixosSystem {
    inherit specialArgs;
    inherit system;
    modules =
      modulesFunc
      [(import ./desktop/home-manager.nix)]
      [(import ./desktop/configuration.nix) (import ./desktop/hardware-configuration.nix)];
  };

  laptop = nixpkgs.lib.nixosSystem {
    inherit specialArgs;
    inherit system;
    modules =
      modulesFunc
      [(import ./laptop/home-manager.nix)]
      [(import ./laptop/configuration.nix) (import ./laptop/hardware-configuration.nix)];
  };
}
