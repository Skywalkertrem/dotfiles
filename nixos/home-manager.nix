{
  config,
  pkgs,
  user,
  ...
}: {
  home = {
    homeDirectory = "/home/${user}";
    packages = with pkgs; [
      docker
      neofetch
      unzip
      wine
      zip
    ];
    username = "${user}";
    stateVersion = "22.05";
  };

  imports =
    (import ../modules/command-line)
    ++ (import ../modules/editors)
    ++ (import ../modules/shells)
    ++ (import ../modules/terminal-emulators);

  programs.home-manager.enable = true;
}
