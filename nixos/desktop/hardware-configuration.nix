{
  config,
  lib,
  modulesPath,
  pkgs,
  ...
}: {
  boot = {
    initrd.availableKernelModules = ["ata_piix" "ohci_pci" "sd_mod" "sr_mod" ];
    kernelModules = [];
  };
  fileSystems."/" =
    { device = "/dev/disk/by-uuid/f4e29323-ecce-4bf3-8bc2-6b3231c82b46";
      fsType = "ext4";
    };


  hardware.cpu.amd.updateMicrocode = lib.mkDefault config.hardware.enableRedistributableFirmware;

  imports = [
    (modulesPath + "/installer/scan/not-detected.nix")
  ];
  virtualisation.virtualbox.guest.enable = true;
}
