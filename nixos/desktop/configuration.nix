{
  config,
  nixpkgs,
  pkgs,
  user,
  ...
}: {
  boot.loader.grub.enable = true;
  boot.loader.grub.device = "/dev/sda";
  boot.loader.grub.useOSProber = true;


  networking.hostName = "desktop";

  #steam test

  # programs.steam = {
  #   dedicatedServer.openFirewall = true; # Open ports in the firewall for Source Dedicated Server
  #   enable = true;
  #   remotePlay.openFirewall = true; # Open ports in the firewall for Steam Remote Play
  # };
}
