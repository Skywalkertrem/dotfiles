{pkgs, ...}: {
  home = {
    packages = with pkgs; [
      flameshot
      vlc
      htop
      # School

      octave
      speedcrunch
    ];
  };

  imports = import ../../modules/internet;
}
