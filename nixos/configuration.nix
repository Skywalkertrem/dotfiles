{
  config,
  nixpkgs,
  pkgs,
  user,
  ...
}: {
  environment.systemPackages = with pkgs; [];

  imports =
    [(import ../modules/sound)]
    ++ [(import ../modules/x11)];

  i18n.defaultLocale = "en_CA.utf8";

  networking.networkmanager.enable = true;

  nix = {
    extraOptions = ''
      experimental-features = nix-command flakes
      keep-outputs = true
      keep-derivations = true
    '';
    gc = {
      automatic = true;
      dates = "weekly";
      options = "--delete-older-than 30d";
    };
    package = pkgs.nixFlakes;
    registry.nixpkgs.flake = nixpkgs;
  };

  nixpkgs.config.allowUnfree = true;

  programs.dconf.enable = true;

  services.printing.enable = true;

  time.timeZone = "America/Toronto";

  users.users.${user} = {
    extraGroups = ["networkmanager" "wheel"];
    isNormalUser = true;
    shell = pkgs.zsh;
  };

  system.stateVersion = "22.05";

  virtualisation.docker.enable = true;
}
