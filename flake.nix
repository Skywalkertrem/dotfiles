{
  description = "The Flake Of Lord Isaac The Kind";

  inputs = {
    home-manager = {
      inputs.nixpkgs.follows = "nixpkgs";
      url = "github:nix-community/home-manager";
    };

    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
  };

  outputs = {
    home-manager,
    nixpkgs,
    self,
  }: let
    pkgs = nixpkgs.legacyPackages.x86_64-linux;
    user = "luket";
  in {
    nixosConfigurations = (
      import ./nixos {
        inherit home-manager nixpkgs user;
      }
    );
  };
}
